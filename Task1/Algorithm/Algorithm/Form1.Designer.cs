﻿namespace Algorithm
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.MetricButton = new System.Windows.Forms.Button();
            this.RerunButton = new System.Windows.Forms.Button();
            this.neigboursButton = new System.Windows.Forms.Button();
            this.yScrool = new System.Windows.Forms.VScrollBar();
            this.xScrool = new System.Windows.Forms.VScrollBar();
            this.progress = new System.Windows.Forms.ProgressBar();
            this.OpenButton = new System.Windows.Forms.Button();
            this.hugeWindowValue = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.smallWindowValue = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.k2value = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.k1value = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.gammaValue = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.alpha1Value = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.container = new System.Windows.Forms.SplitContainer();
            this.ofd = new System.Windows.Forms.OpenFileDialog();
            this.tmr = new System.Windows.Forms.Timer(this.components);
            this.precision = new System.Windows.Forms.Label();
            this.recall = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hugeWindowValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smallWindowValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.k2value)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.k1value)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gammaValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alpha1Value)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.container)).BeginInit();
            this.container.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.recall);
            this.groupBox1.Controls.Add(this.precision);
            this.groupBox1.Controls.Add(this.MetricButton);
            this.groupBox1.Controls.Add(this.RerunButton);
            this.groupBox1.Controls.Add(this.neigboursButton);
            this.groupBox1.Controls.Add(this.yScrool);
            this.groupBox1.Controls.Add(this.xScrool);
            this.groupBox1.Controls.Add(this.progress);
            this.groupBox1.Controls.Add(this.OpenButton);
            this.groupBox1.Controls.Add(this.hugeWindowValue);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.smallWindowValue);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.k2value);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.k1value);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.gammaValue);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.alpha1Value);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(150, 625);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Tag = "";
            this.groupBox1.Text = "Управление";
            // 
            // MetricButton
            // 
            this.MetricButton.Enabled = false;
            this.MetricButton.Location = new System.Drawing.Point(53, 371);
            this.MetricButton.Name = "MetricButton";
            this.MetricButton.Size = new System.Drawing.Size(91, 59);
            this.MetricButton.TabIndex = 7;
            this.MetricButton.Text = "Metric";
            this.MetricButton.UseVisualStyleBackColor = true;
            this.MetricButton.Click += new System.EventHandler(this.MetricButton_Click);
            // 
            // RerunButton
            // 
            this.RerunButton.Enabled = false;
            this.RerunButton.Location = new System.Drawing.Point(53, 309);
            this.RerunButton.Name = "RerunButton";
            this.RerunButton.Size = new System.Drawing.Size(91, 56);
            this.RerunButton.TabIndex = 6;
            this.RerunButton.Text = "ReRun";
            this.RerunButton.UseVisualStyleBackColor = true;
            this.RerunButton.Click += new System.EventHandler(this.RerunButton_Click);
            // 
            // neigboursButton
            // 
            this.neigboursButton.Location = new System.Drawing.Point(53, 235);
            this.neigboursButton.Name = "neigboursButton";
            this.neigboursButton.Size = new System.Drawing.Size(91, 67);
            this.neigboursButton.TabIndex = 5;
            this.neigboursButton.Text = "Show neighbours";
            this.neigboursButton.UseVisualStyleBackColor = true;
            this.neigboursButton.Click += new System.EventHandler(this.neigboursButton_Click);
            // 
            // yScrool
            // 
            this.yScrool.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.yScrool.Location = new System.Drawing.Point(33, 235);
            this.yScrool.Name = "yScrool";
            this.yScrool.Size = new System.Drawing.Size(17, 387);
            this.yScrool.TabIndex = 4;
            this.yScrool.ValueChanged += new System.EventHandler(this.yScool_ValueChanged);
            // 
            // xScrool
            // 
            this.xScrool.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.xScrool.Location = new System.Drawing.Point(7, 235);
            this.xScrool.Name = "xScrool";
            this.xScrool.Size = new System.Drawing.Size(17, 387);
            this.xScrool.TabIndex = 4;
            this.xScrool.ValueChanged += new System.EventHandler(this.yScool_ValueChanged);
            // 
            // progress
            // 
            this.progress.Location = new System.Drawing.Point(6, 205);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(138, 23);
            this.progress.TabIndex = 3;
            // 
            // OpenButton
            // 
            this.OpenButton.Location = new System.Drawing.Point(6, 176);
            this.OpenButton.Name = "OpenButton";
            this.OpenButton.Size = new System.Drawing.Size(138, 23);
            this.OpenButton.TabIndex = 2;
            this.OpenButton.Text = "Open";
            this.OpenButton.UseVisualStyleBackColor = true;
            this.OpenButton.Click += new System.EventHandler(this.OpenButton_Click);
            // 
            // hugeWindowValue
            // 
            this.hugeWindowValue.Location = new System.Drawing.Point(33, 150);
            this.hugeWindowValue.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.hugeWindowValue.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.hugeWindowValue.Name = "hugeWindowValue";
            this.hugeWindowValue.Size = new System.Drawing.Size(111, 20);
            this.hugeWindowValue.TabIndex = 1;
            this.hugeWindowValue.Value = new decimal(new int[] {
            120,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "hw";
            // 
            // smallWindowValue
            // 
            this.smallWindowValue.Location = new System.Drawing.Point(33, 124);
            this.smallWindowValue.Maximum = new decimal(new int[] {
            1050,
            0,
            0,
            0});
            this.smallWindowValue.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.smallWindowValue.Name = "smallWindowValue";
            this.smallWindowValue.Size = new System.Drawing.Size(111, 20);
            this.smallWindowValue.TabIndex = 1;
            this.smallWindowValue.Value = new decimal(new int[] {
            37,
            0,
            0,
            0});
            this.smallWindowValue.ValueChanged += new System.EventHandler(this.smallWindowValue_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "sw";
            // 
            // k2value
            // 
            this.k2value.DecimalPlaces = 2;
            this.k2value.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.k2value.Location = new System.Drawing.Point(33, 98);
            this.k2value.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.k2value.Name = "k2value";
            this.k2value.Size = new System.Drawing.Size(111, 20);
            this.k2value.TabIndex = 1;
            this.k2value.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(19, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "k2";
            // 
            // k1value
            // 
            this.k1value.DecimalPlaces = 2;
            this.k1value.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.k1value.Location = new System.Drawing.Point(33, 72);
            this.k1value.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.k1value.Name = "k1value";
            this.k1value.Size = new System.Drawing.Size(111, 20);
            this.k1value.TabIndex = 1;
            this.k1value.Value = new decimal(new int[] {
            12,
            0,
            0,
            131072});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "k1";
            // 
            // gammaValue
            // 
            this.gammaValue.DecimalPlaces = 2;
            this.gammaValue.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.gammaValue.Location = new System.Drawing.Point(33, 46);
            this.gammaValue.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.gammaValue.Name = "gammaValue";
            this.gammaValue.Size = new System.Drawing.Size(111, 20);
            this.gammaValue.TabIndex = 1;
            this.gammaValue.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "γ";
            // 
            // alpha1Value
            // 
            this.alpha1Value.DecimalPlaces = 2;
            this.alpha1Value.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.alpha1Value.Location = new System.Drawing.Point(33, 20);
            this.alpha1Value.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.alpha1Value.Name = "alpha1Value";
            this.alpha1Value.Size = new System.Drawing.Size(111, 20);
            this.alpha1Value.TabIndex = 1;
            this.alpha1Value.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "a1";
            // 
            // container
            // 
            this.container.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.container.ForeColor = System.Drawing.Color.Lime;
            this.container.Location = new System.Drawing.Point(168, 12);
            this.container.Name = "container";
            this.container.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // container.Panel1
            // 
            this.container.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel1_Paint);
            // 
            // container.Panel2
            // 
            this.container.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel2_Paint);
            this.container.Size = new System.Drawing.Size(991, 625);
            this.container.SplitterDistance = 309;
            this.container.TabIndex = 1;
            // 
            // ofd
            // 
            this.ofd.Filter = "Images BMP (*.bmp) | *.bmp";
            this.ofd.Title = "Select image";
            // 
            // tmr
            // 
            this.tmr.Tick += new System.EventHandler(this.tmr_Tick);
            // 
            // precision
            // 
            this.precision.AutoSize = true;
            this.precision.Location = new System.Drawing.Point(54, 437);
            this.precision.Name = "precision";
            this.precision.Size = new System.Drawing.Size(35, 13);
            this.precision.TabIndex = 8;
            this.precision.Text = "label7";
            // 
            // recall
            // 
            this.recall.AutoSize = true;
            this.recall.Location = new System.Drawing.Point(53, 450);
            this.recall.Name = "recall";
            this.recall.Size = new System.Drawing.Size(35, 13);
            this.recall.TabIndex = 8;
            this.recall.Text = "label7";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1171, 649);
            this.Controls.Add(this.container);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.Text = "Binarization";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hugeWindowValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smallWindowValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.k2value)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.k1value)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gammaValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alpha1Value)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.container)).EndInit();
            this.container.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown alpha1Value;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown k2value;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown k1value;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown gammaValue;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.SplitContainer container;
        private System.Windows.Forms.OpenFileDialog ofd;
        private System.Windows.Forms.ProgressBar progress;
        private System.Windows.Forms.Button OpenButton;
        private System.Windows.Forms.NumericUpDown hugeWindowValue;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown smallWindowValue;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Timer tmr;
        private System.Windows.Forms.VScrollBar yScrool;
        private System.Windows.Forms.VScrollBar xScrool;
        private System.Windows.Forms.Button neigboursButton;
        private System.Windows.Forms.Button RerunButton;
        private System.Windows.Forms.Button MetricButton;
        private System.Windows.Forms.Label recall;
        private System.Windows.Forms.Label precision;
    }
}

