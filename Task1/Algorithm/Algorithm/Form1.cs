﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Algorithm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private Worker worker;

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {
            if (worker != null)
                lock (worker)
                {
                    int x = -xScrool.Value;
                    int y = -yScrool.Value;
                    Graphics g = e.Graphics;
                    g.DrawImageUnscaled(worker.Original, x, y);
                }
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {
            if (worker != null)
                lock (worker)
                {
                    int x = -xScrool.Value;
                    int y = -yScrool.Value;
                    Graphics g = e.Graphics;
                    g.DrawImageUnscaled(worker.Binarized, x, y);
                }
        }

        private void OpenButton_Click(object sender, EventArgs e)
        {
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                Image original = Bitmap.FromFile(ofd.FileName);
                worker = new Worker(original);
                double alpha1 = (double)alpha1Value.Value;
                double k1 = (double)k1value.Value;
                double k2 = (double)k2value.Value;
                double gamma = (double)gammaValue.Value;
                int sw = (int)smallWindowValue.Value;
                int hw = (int)hugeWindowValue.Value;
                string filename = ofd.FileName;
                xScrool.Maximum = original.Width;
                yScrool.Maximum = original.Height;
                RerunButton.Enabled = false;

                Thread thread = new Thread(() => { worker.Work(alpha1, gamma, k1, k2, filename, sw, hw); });
                thread.IsBackground = true;
                thread.Start();
                Graphics g = container.Panel1.CreateGraphics();
                g.Clear(container.BackColor);
                g.Dispose();
                g = container.Panel2.CreateGraphics();
                g.Clear(container.BackColor);
                g.Dispose();
            }
        }

        private void smallWindowValue_ValueChanged(object sender, EventArgs e)
        {
            hugeWindowValue.Minimum = smallWindowValue.Value;
        }

        private void tmr_Tick(object sender, EventArgs e)
        {
            Redraw();

            if (worker == null)
                return;
            RerunButton.Enabled = Math.Abs(worker.Completed - 1.0f) < float.Epsilon;
            MetricButton.Enabled = Math.Abs(worker.Completed - 1.0f) < float.Epsilon;
        }

        private void Redraw()
        {
            if (worker != null)
                lock (worker)
                {
                    int x = -xScrool.Value;
                    int y = -yScrool.Value;
                    Graphics g = container.Panel1.CreateGraphics();
                    g.DrawImageUnscaled(worker.Original, x, y);
                    g.Dispose();
                    g = container.Panel2.CreateGraphics();
                    g.DrawImageUnscaled(worker.Binarized, x, y);
                    g.Dispose();
                    progress.Value = (int)(worker.Completed * 100);
                }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            tmr.Start();
        }

        private void yScool_ValueChanged(object sender, EventArgs e)
        {
            /*if (worker != null)
                lock (worker)
                {
                    int x = -xScrool.Value;
                    int y = -yScrool.Value;
                    Graphics g = container.Panel1.CreateGraphics();
                    g.Clear(container.BackColor);
                    g.DrawImageUnscaled(worker.Original, x, y);
                    g.Dispose();
                    g = container.Panel2.CreateGraphics();
                    g.Clear(container.BackColor);
                    g.DrawImageUnscaled(worker.Binarized, x, y);
                    g.Dispose();
                    progress.Value = (int)(worker.Completed * 100);
                }*/
            Redraw();
        }

        private void neigboursButton_Click(object sender, EventArgs e)
        {
            Graphics g = container.Panel1.CreateGraphics();
            g.DrawRectangle(Pens.Red, container.Panel1.ClientSize.Width / 2 - (float)smallWindowValue.Value,
                container.Panel1.ClientSize.Height / 2 - (float)smallWindowValue.Value, 2 * (float)smallWindowValue.Value + 1,
                2 * (float)smallWindowValue.Value + 1);
            g.DrawRectangle(Pens.Green, container.Panel1.ClientSize.Width / 2 - (float)hugeWindowValue.Value,
                container.Panel1.ClientSize.Height / 2 - (float)hugeWindowValue.Value, 2 * (float)hugeWindowValue.Value + 1,
                2 * (float)hugeWindowValue.Value + 1);
            g.Dispose();
        }

        private void RerunButton_Click(object sender, EventArgs e)
        {
            double alpha1 = (double)alpha1Value.Value;
            double k1 = (double)k1value.Value;
            double k2 = (double)k2value.Value;
            double gamma = (double)gammaValue.Value;
            int sw = (int)smallWindowValue.Value;
            int hw = (int)hugeWindowValue.Value;
            Thread thread = new Thread(() => { worker.ReRun(alpha1, gamma, k1, k2, sw, hw); });
            thread.IsBackground = true;
            thread.Start();
        }

        private void MetricButton_Click(object sender, EventArgs e)
        {
            new Thread(() =>
            {
                Process proc = new Process();
                proc.StartInfo = new ProcessStartInfo(@"D:\Projects\MIPT\OCR\Task1\Weight\BinEvalWeights.exe", worker.Filename.Replace("OriginalImages", "GTimages").
                    Replace(".tiff", "_estGT.tiff").Replace(".bmp", "_estGT.tiff"));
                proc.StartInfo.WorkingDirectory = @"D:\Projects\MIPT\OCR\Task1\Weight";
                File.WriteAllText("run.bat", proc.StartInfo.FileName + " " + proc.StartInfo.Arguments);
                proc.StartInfo.CreateNoWindow = true;
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.RedirectStandardError = true;
                proc.StartInfo.RedirectStandardInput = true;
                proc.Start();
                proc.WaitForExit();
                string line;
                proc = new Process();
                proc.StartInfo = new ProcessStartInfo(@"D:\Projects\MIPT\OCR\Task1\Metric\DIBCO13_metrics.exe", worker.Filename.Replace("OriginalImages", "GTimages")
                    .Replace(".tiff", "_estGT.tiff").Replace(".bmp", "_estGT.tiff") + " " + worker.Filename.Replace("OriginalImages", "Result")
                        + " " + worker.Filename.Replace("OriginalImages", "GTimages").Replace(".tiff", "_estGT_RWeights.dat").Replace(".bmp", "_estGT_RWeights.dat") +
                        " " + worker.Filename.Replace("OriginalImages", "GTimages").Replace(".tiff", "_estGT_PWeights.dat").Replace(".bmp", "_estGT_PWeights.dat"));
                File.WriteAllText("run.bat", proc.StartInfo.FileName + " " + proc.StartInfo.Arguments);
                proc.StartInfo.CreateNoWindow = true;
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.RedirectStandardError = true;
                proc.StartInfo.RedirectStandardInput = true;
                proc.Start();
                proc.WaitForExit();
                line = proc.StandardOutput.ReadLine();
                this.Invoke(new Action(() =>
                {
                    MessageBox.Show(line, "Metric");
                    string[] tokens = line.Split();
                    precision.Text = "P " + tokens[5];
                    recall.Text = "R " + tokens[4];
                }));
            }).Start();
        }
    }
}
