﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;

namespace Algorithm
{
    class Worker
    {
        internal Bitmap Original;
        internal Bitmap Binarized;
        internal volatile float Completed;
        private double[,] Grayscale;
        private SequenceTree[] Trees;
        internal string Filename { get; private set; }

        internal Worker(Image original)
        {
            Original = (Bitmap)original;
            Binarized = new Bitmap(original.Width, original.Height);
            Completed = 0;
        }

        internal void Work(double alpha1, double gamma, double k1, double k2, string filename, int smallSize, int hugeSize)
        {
            this.Filename = filename;
            int width;
            int height;

            lock (this)
            {
                width = Original.Width;
                height = Original.Height;

                Grayscale = GetGrayScale(width, height);
            }

            lock (this)
            {
                PaintTempColor(width, height);
            }

#if DEBUG
            SaveDebugGray(width, height);
#endif //DEBUG

            Trees = BuildMinTrees(smallSize, width, height);

            ReRun(alpha1, gamma, k1, k2, smallSize, hugeSize);
        }

        internal void ReRun(double alpha1, double gamma, double k1, double k2, int smallSize, int hugeSize)
        {
            int width;
            int height;
            lock (this)
            {
                width = Original.Width;
                height = Original.Height;
            }

            Binarize(alpha1, gamma, k1, k2, smallSize, hugeSize, Grayscale, width, height, Trees);

            lock (this)
            {
                Save(Filename, String.Format("k1={0} k2={1} alpha1={2} gamma={3} smallWindow={4} hugeWindow = {5}",
                    k1, k2, alpha1, gamma, smallSize, hugeSize));
            }

            Completed = 1.0f;
        }

        private SequenceTree[] BuildMinTrees(int smallSize, int width, int height)
        {
            SequenceTree[] trees = new SequenceTree[width];
            for (int i = 0; i < width; i++)
            {
                trees[i] = new SequenceTree(Grayscale, height, i);
            }
            double[][] mins = new double[height][];
            for (int y = 0; y < height; y++)
            {
                mins[y] = new double[width];
                for (int x = 0; x < width; x++)
                {
                    int windowBeginY = Math.Max(0, y - smallSize);
                    int windowEndY = Math.Min(height - 1, y + smallSize);
                    mins[y][x] = trees[x].Request(windowBeginY, windowEndY);
                }
                Completed = y / 3.0f / width;

            }
            trees = new SequenceTree[height];
            for (int y = 0; y < height; y++)
            {
                trees[y] = new SequenceTree(mins[y], width);
                Completed = 1 / 3.0f + y / 3.0f / width;
            }

            return trees;
        }

        private void SaveDebugGray(int width, int height)
        {
            Bitmap gray = new Bitmap(width, height);
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    gray.SetPixel(i, j, Color.FromArgb((int)Grayscale[i, j], (int)Grayscale[i, j], (int)Grayscale[i, j]));
                }
            }
            var tokens = Filename.Split("\\".ToCharArray());
            gray.Save("DEBUG_GRAY_" + tokens[tokens.Length - 1]);
        }

        private void Binarize(double alpha1, double gamma, double k1, double k2, int smallSize, int hugeSize, double[,] grayscale, int width, int height, SequenceTree[] trees)
        {
            SumHolder sums = new SumHolder(grayscale, width, height, x => x);
            SumHolder sqSums = new SumHolder(grayscale, width, height, x => x * x);

            bool[] line = new bool[height];
#if DEBUG
            double[] tresholds = new double[height];
            Bitmap tresholdCode = new Bitmap(width, height);
#endif //DEBUG

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    double mean, deviation, hugeDeviation;
                    ProcessNeighbour(hugeSize, grayscale, width, height, i, j, sums, sqSums, out mean, out hugeDeviation);
                    ProcessNeighbour(smallSize, grayscale, width, height, i, j, sums, sqSums, out mean, out deviation);

                    double min = SearchMin(width, trees, j, smallSize, i);

                    double deviationRatio = deviation / hugeDeviation;
                    if (double.IsNaN(deviationRatio))
                        deviationRatio = 1;
                    if (hugeDeviation == 0)
                        deviationRatio = 1;
                    double alpha2 = k1 * Math.Pow(deviationRatio, (gamma + 1) / 2.0);
                    double alpha3 = k2 * Math.Pow(deviationRatio, gamma / 2.0);
                    double treshold = (1 - alpha1) * mean + alpha2 * (mean - min) + alpha3 * min;
                    line[j] = grayscale[i, j] >= treshold;
                    if (double.IsNaN(treshold))
                        treshold = 0;
#if DEBUG
                    tresholds[j] = treshold;
#endif //DEBUG
                }
                lock (this)
                {
                    for (int j = 0; j < height; j++)
                        Binarized.SetPixel(i, j, line[j] ? Color.White : Color.Black);
#if DEBUG
                    for (int j = 0; j < height; j++)
                    {
                        if (tresholds[j] < 0)
                            tresholdCode.SetPixel(i, j, Color.Green);
                        else
                        if (tresholds[j] > 255)
                            tresholdCode.SetPixel(i, j, Color.Yellow);
                        else
                            tresholdCode.SetPixel(i, j, Color.FromArgb((int)tresholds[j], 0, 0));
                    }
#endif //DEBUG
                }
                Completed = 2.0f / 3 + i / 3.0f / width;
            }

#if DEBUG
            tresholdCode.Save("DEBUG.bmp");
#endif //DEBUG
        }

        private static double SearchMin(int width, SequenceTree[] trees, int y, int radius, int x)
        {
            double min;
            int windowBeginX = Math.Max(0, x - radius);
            int windowEndX = Math.Min(width - 1, x + radius);
            min = trees[y].Request(windowBeginX, windowEndX);
            return min;
        }

        private static void ProcessNeighbour(int radius, double[,] grayscale, int width, int height, int x, int y,
            SumHolder sums, SumHolder sqSums, out double mean, out double deviation)
        {
            int windowBeginX = Math.Max(0, x - radius);
            int windowBeginY = Math.Max(0, y - radius);
            int windowEndX = Math.Min(width - 1, x + radius);
            int windowEndY = Math.Min(height - 1, y + radius);
            int total = (windowEndY - windowBeginY + 1) * (windowEndX - windowBeginX + 1);
            mean = sums.Request(windowBeginX, windowBeginY, windowEndX, windowEndY) / total;
            double sqMean = sqSums.Request(windowBeginX, windowBeginY, windowEndX, windowEndY) / total;
            deviation = sqMean - mean * mean;
        }

        private void PaintTempColor(int width, int height)
        {
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Binarized.SetPixel(i, j, Color.Blue);
                }
            }
        }

        private double[,] GetGrayScale(int width, int height)
        {
            double[,] grayscale = new double[width, height];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    var color = Original.GetPixel(i, j);
                    grayscale[i, j] = (0.299 * color.R + 0.587 * color.G + 0.114 * color.B);
                }
            }

            return grayscale;
        }

        private void Save(string Filename, string param)
        {
            var tokens = Filename.Split("\\".ToCharArray());
            Binarized.Save("..\\..\\..\\..\\..\\Result\\" + tokens[tokens.Length - 1]);
            File.WriteAllText("..\\..\\..\\..\\..\\Result\\" + tokens[tokens.Length - 1] + ".txt", param);
        }
    }
}
