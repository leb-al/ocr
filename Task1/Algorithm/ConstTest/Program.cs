﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Algorithm;
using System.Threading;
using System.IO;
using System.Drawing;

namespace ConstTest
{
    class Program
    {
        static void Main(string[] args)
        {
            double k1 = 0.05;
            int sw = 30;
            int hw = 100;
            double k2 = 0.01;

            int added = 0;
            int comleted = 0;
            int active = 0;
            Object o = new object();
            for (double a = 0.2; a < 0.41; a += 0.05)
            {
                var files = Directory.GetFiles(@"D:\Projects\MIPT\OCR\Task1\OriginalImages");
                foreach (var item in files)
                {
                    double al = a;
                    lock (o)
                    {
                        ++active;
                    }
                    ++added;
                    Thread thread = new Thread(() =>
                    {
                        Console.WriteLine("Reading {0} with a {1}", item, al);
                        Worker w = new Worker(Image.FromFile(item));
                        w.Work(al, 2, k1, k2, item, sw, hw);
                        Console.WriteLine("Finish {0} with a {1}", item, al);
                        lock (o)
                        {
                            ++comleted;
                            Console.WriteLine("Done {0} form {1} ({2} %)", comleted, added, (int)(comleted * 100.0 / added));
                            --active;
                        }
                    });
                    thread.Start();
                    while (true)
                    {
                        lock (o)
                        {
                            if (active < 4)
                                break;
                        }
                        Thread.Sleep(5000);
                    }
                }
            }
        }
    }
}
