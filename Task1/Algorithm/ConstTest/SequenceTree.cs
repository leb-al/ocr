﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Algorithm
{
    class SequenceTree
    {
        private double[] Data;
        private int Size;

        internal SequenceTree(double[,] values, int size, int j)
        {
            Data = new double[size * 4];
            Size = size;
            build(values, j, 1, 0, size - 1);
        }

        internal SequenceTree(double[] values, int size)
        {
            Data = new double[size * 4];
            Size = size;
            build(values, 1, 0, size - 1);
        }

        private void build(double[,] values, int j, int v, int tl, int tr)
        {
            if (tl == tr)
                Data[v] = values[j, tl];
            else {
                int tm = (tl + tr) / 2;
                build(values, j, v * 2, tl, tm);
                build(values, j, v * 2 + 1, tm + 1, tr);
                Data[v] = Math.Min(Data[v * 2], Data[v * 2 + 1]);
            }
        }

        private void build(double[] values, int v, int tl, int tr)
        {
            if (tl == tr)
                Data[v] = values[tl];
            else {
                int tm = (tl + tr) / 2;
                build(values, v * 2, tl, tm);
                build(values, v * 2 + 1, tm + 1, tr);
                Data[v] = Math.Min(Data[v * 2], Data[v * 2 + 1]);
            }
        }

        double Work(int v, int tl, int tr, int l, int r)
        {
            if (l > r)
                return 0;
            if (l == tl && r == tr)
                return Data[v];
            int tm = (tl + tr) / 2;
            return Math.Min(Work(v * 2, tl, tm, l, Math.Min(r, tm)), Work(v * 2 + 1, tm + 1, tr, Math.Max(l, tm + 1), r));
        }

        internal double Request(int l, int r)
        {

            return Work(1, 0, Size - 1, l, r);
        }
    }

    class SumHolder
    {
        private double[,] sum;

        internal SumHolder(double[,] array, int width, int height, Func f)
        {
            sum = new double[width, height];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    sum[i, j] = f(array[i, j]) - Request(0, 0, i - 1, j - 1) + Request(0, 0, i - 1, j) + Request(0, 0, i, j - 1);
                }
            }
        }

        internal double Request(int beginX, int beginY, int endX, int endY)
        {
            if ((endX < beginX) || (endY < beginY))
            {
                return 0;
            }
            if (beginX == 0 && beginY == 0)
            {
                return sum[endX, endY];
            }
            return sum[endX, endY] + Request(0, 0, beginX - 1, beginY - 1) - Request(0, 0, beginX - 1, endY) - Request(0, 0, endX, beginY - 1);
        }

        internal delegate double Func(double x);
    }
}
